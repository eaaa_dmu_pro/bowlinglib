﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingLib.Model
{
    public class Strike : Frame
    {
        private int? _firstExtraPoints;
        private int? _secondExtraPoints;

        public override int? FrameTotal { get
            {
                if (_firstExtraPoints.HasValue && _secondExtraPoints.HasValue)
                {
                    return 10 + _firstExtraPoints.Value + _secondExtraPoints.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public override int AddFirstScoreFromNextFrame(int score)
        {
            _firstExtraPoints = score;
            return 0;
        }

        public override int AddSecondScoreFromNextFrame(int score)
        {
            _secondExtraPoints = score;
            return FrameTotal.Value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingLib.Model
{
    public class OpenFrame : Frame
    {
        public int FirstRoll { get; }
        public int SecondRoll { get; }
        public override int? FrameTotal => FirstRoll + SecondRoll;

        public OpenFrame(int first, int second)
        {
            FirstRoll = first;
            SecondRoll = second;
        }

        public override int AddFirstScoreFromNextFrame(int score)
        {
            return 0;
        }

        public override int AddSecondScoreFromNextFrame(int score)
        {
            return 0;
        }
    }
}

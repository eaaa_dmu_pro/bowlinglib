﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingLib.Model
{
    public abstract class Frame
    {
        public abstract int AddFirstScoreFromNextFrame(int score);
        public abstract int AddSecondScoreFromNextFrame(int score);
        public abstract int? FrameTotal { get; }
    }
}

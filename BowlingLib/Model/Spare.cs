﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingLib.Model
{
    public class Spare : Frame
    {
        private int? _extraPoints;
        private int _firstRoll;
        public override int? FrameTotal { get
            {
                if (_extraPoints.HasValue)
                {
                    return 10 + _extraPoints.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public Spare(int pins)
        {
            _firstRoll = pins;
        }


        public override int AddFirstScoreFromNextFrame(int score)
        {
            _extraPoints = score;
            return FrameTotal.Value;
        }

        public override int AddSecondScoreFromNextFrame(int score)
        {
            return 0;
        }
    }
}

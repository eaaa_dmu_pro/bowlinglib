﻿using BowlingLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingLib
{
    public class Game
    {
        private Frame[] _frames;
        private int _currentFrame;
        private int? _firstRollInFrame;

        public int TotalScore { get; private set; }

        public Game()
        {
            _frames = new Frame[10];
            _currentFrame = 0;
        }

        public Frame GetFrame(int frameNumber)
        {
            if (frameNumber < 0 || frameNumber > 9)
            {
                throw new ArgumentException($"Frame {frameNumber} does not exits.");
            }

            return _frames[frameNumber];
        }

        public void Roll(int pins)
        {
            if (pins < 0 || pins > 10)
            {
                throw new ArgumentException("pins should be between 0 and 10");
            }

            if (IsFirstRollInFrame())
            {
                int change = AddFirstRollToPreviousFrame(pins);
                UpdateTotalScore(change);
                if (IsStrike())
                {
                    _frames[_currentFrame] = new Strike();
                    IncrementFrame();
                }
                else
                {
                    _firstRollInFrame = pins;
                }
            }
            else //Second roll in Frame
            {
                int change = AddSecondRollToPreviousFrame(pins);
                UpdateTotalScore(change);
                if (IsSpare())
                {
                    _frames[_currentFrame] = new Spare(_firstRollInFrame.Value);
                    IncrementFrame();
                }
                else
                {
                    _frames[_currentFrame] = new OpenFrame(_firstRollInFrame.Value, pins);
                    TotalScore = TotalScore + _frames[_currentFrame].FrameTotal.Value;
                    IncrementFrame();
                }
            }

            bool IsStrike()
            {
                return pins == 10;
            }

            bool IsSpare()
            {
                return pins + _firstRollInFrame == 10;
            }
        }

        private bool IsFirstRollInFrame()
        {
            return !_firstRollInFrame.HasValue;
        }

        private void UpdateTotalScore(int change)
        {
            TotalScore = TotalScore + change;
        }

        private int AddFirstRollToPreviousFrame(int pins)
        {
            if (_currentFrame > 0)
            {
                return _frames[_currentFrame - 1].AddFirstScoreFromNextFrame(pins);
            }
            else
            {
                return 0;
            }
        }

        private int AddSecondRollToPreviousFrame(int pins)
        {
            if (_currentFrame > 0)
            {
                return _frames[_currentFrame - 1].AddSecondScoreFromNextFrame(pins);
            }
            else
            {
                return 0;
            }
        }

        private void IncrementFrame()
        {
            if (_currentFrame == 10) { }
            else
            {
                _currentFrame = _currentFrame + 1;
                _firstRollInFrame = null;
            }
        }
    }
}

﻿using BowlingLib;
using BowlingLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BowlingLibUnittests
{
    public class BowlingGameUnitTest
    {
        [Fact]
        public void Should_ThrowException_When_NumberOfPinsIsOutsideRange()
        {
            var sut = new Game();
            var actual = Assert.Throws<ArgumentException>(() => sut.Roll(-1));
            Assert.Equal("pins should be between 0 and 10", actual.Message);
            actual = Assert.Throws<ArgumentException>(() => sut.Roll(11));
            Assert.Equal("pins should be between 0 and 10", actual.Message);

        }

        [Fact]
        public void Should_ThrowException_When_FrameNumberIsOutsideRange()
        {
            var sut = new Game();
            int frameNumber = -1;
            var actual = Assert.Throws<ArgumentException>(() => sut.GetFrame(frameNumber));
            Assert.Equal($"Frame {frameNumber} does not exits.", actual.Message);

            frameNumber = 10;
            actual = Assert.Throws<ArgumentException>(() => sut.GetFrame(frameNumber));
            Assert.Equal($"Frame {frameNumber} does not exits.", actual.Message);
        }

        [Fact]
        public void Should_ReturnNull_When_FrameIsNotPlayed()
        {
            var sut = new Game();
            Assert.Null(sut.GetFrame(0));
        }

        [Fact]
        public void Should_CreateOpenFrame_When_LessThan10PinsAreRolled()
        {
            int firstRoll = 2;
            int secondRoll = 4;
            var game = new Game();
            game.Roll(firstRoll);
            game.Roll(secondRoll);
            Assert.Equal(firstRoll + secondRoll, game.GetFrame(0).FrameTotal);
            Assert.Equal(typeof(OpenFrame), game.GetFrame(0).GetType());
        }

        [Fact]
        public void Should_NotAddPointFromSubsequentFrame_When_OpenFrameIsAchieved()
        {
            int firstRoll = 5;
            int secondRoll = 4;
            int thirdRoll = 7;
            int forthRoll = 0;
            var game = new Game();
            game.Roll(firstRoll);
            game.Roll(secondRoll);
            game.Roll(thirdRoll);
            Assert.Equal(firstRoll + secondRoll, game.GetFrame(0).FrameTotal);
            game.Roll(forthRoll);
            Assert.Equal(firstRoll + secondRoll + thirdRoll, game.TotalScore);
        }

        [Fact]
        public void Should_AddPointFromNextRoll_When_SpareIsAchieved()
        {
            int firstRoll = 6;
            int secondRoll = 4;
            int thirdRoll = 7;
            int forthRoll = 1;
            var game = new Game();
            game.Roll(firstRoll);
            game.Roll(secondRoll);
            game.Roll(thirdRoll);
            Assert.Equal(firstRoll + secondRoll + thirdRoll, game.GetFrame(0).FrameTotal);
            game.Roll(forthRoll);
            Assert.Equal(firstRoll + secondRoll + 2 * thirdRoll + forthRoll, game.TotalScore);
        }

        [Fact]
        public void Should_AddPointsPointsFromNextTwoRolls_When_StrikeIsAchieved()
        {
            int firstRoll = 10;
            int secondRoll = 4;
            int thirdRoll = 5;
            var game = new Game();
            game.Roll(firstRoll);
            Assert.Equal(typeof(Strike), game.GetFrame(0).GetType());
            game.Roll(secondRoll);
            game.Roll(thirdRoll);
            Assert.Equal(firstRoll + secondRoll + thirdRoll, game.GetFrame(0).FrameTotal);
            Assert.Equal(firstRoll + 2 * secondRoll + 2 * thirdRoll, game.TotalScore);
        }

        [Fact]
        public void Should_Give30PointsOnFirstStrike_When_ATurkeyIsAchieved()
        {
            int firstRoll = 10;
            int secondRoll = 10;
            int thirdRoll = 10;
            var game = new Game();
            game.Roll(firstRoll);
            Assert.Equal(typeof(Strike), game.GetFrame(0).GetType());
            game.Roll(secondRoll);
            game.Roll(thirdRoll);
            Assert.Equal(firstRoll + secondRoll + thirdRoll, game.GetFrame(0).FrameTotal);
        }

        [Fact]
        public void Should_Give300Score_When_PerfectGameIsAchieved()
        {
            var game = new Game();
            for (int i = 0; i < 11; i++)
            {
                game.Roll(10);
            }
            Assert.Equal(300, game.TotalScore);
        }
    }
}
